include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

TARGETS = mat2xpm mat2sng

all: ${TARGETS}
	${RM} *.vtu

mat2xpm: mat2xpm.o
	${CLINKER} -o $@ $^ ${PETSC_LIB}

mat2sng: mat2sng.o
	${CLINKER} -o $@ $^ ${PETSC_LIB}

clean::
	${RM} ${TARGETS} *.vtu *~
