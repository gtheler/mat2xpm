// based on mat/examples/tutorials/ex16.c
static char help[] = "\
Usage: ./mat2xpm -f <matrix file> [ -size <size in pixels> ] \n\n\
Reads a matrix from PETSc binary file and saves its structure as an XPM image.\n\
Default size is 640x640 pixels. \n\n";

#include <petscsys.h>
#include <petscmat.h>
#include <petscdraw.h>

#define petsc_call(f) ierr=(f);CHKERRQ(ierr);


#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **args)
{
  Mat               A;
  PetscViewer       fd;
  char              file[PETSC_MAX_PATH_LEN];
  char              image[PETSC_MAX_PATH_LEN];
  char              base[PETSC_MAX_PATH_LEN];
  char              command[PETSC_MAX_PATH_LEN];
  char *            ext;
  PetscInt          size;
  PetscErrorCode    ierr;
  PetscBool         flg;
  PetscMPIInt       rank;

  PetscDraw draw;
  PetscViewer viewer;

  PetscInitialize(&argc,&args,(char*)0,help);
  petsc_call(MPI_Comm_rank(PETSC_COMM_WORLD,&rank));

  petsc_call(PetscOptionsGetString(NULL, "-f", file, PETSC_MAX_PATH_LEN, &flg));
  if (!flg) {
    petsc_call(PetscStrcpy(file, "matrix.bin"));
    petsc_call(PetscStrcpy(image, "matrix.xpm"));
    petsc_call(PetscStrcpy(base, "matrix"));
  } else {
    petsc_call(PetscStrcpy(image, file));
    petsc_call(PetscStrrchr(image, '.', &ext));
    if (ext != NULL) {
      *(ext-1) = '\0';
    }
    petsc_call(PetscStrcpy(base, image));
    *(ext-1) = '.';
    petsc_call(PetscStrcpy(ext, "xpm"));
  }

  petsc_call(PetscOptionsGetInt(NULL, "-size", &size, &flg));
  if (!flg) {
    size = 640;
  }

    
  petsc_call(PetscViewerBinaryOpen(PETSC_COMM_WORLD,file,FILE_MODE_READ,&fd));
  petsc_call(MatCreate(PETSC_COMM_WORLD,&A));
  petsc_call(MatSetFromOptions(A));
  petsc_call(MatLoad(A,fd));
  
  petsc_call(PetscViewerDrawOpen(PETSC_COMM_WORLD, PETSC_NULL, file, PETSC_DECIDE, PETSC_DECIDE, size, size, &viewer));
  petsc_call(PetscViewerDrawGetDraw(viewer, 0, &draw));
  petsc_call(PetscDrawSetFromOptions(draw));  
  petsc_call(PetscDrawSetSaveFinalImage(draw, image));
  petsc_call(MatView(A, viewer));
  
  petsc_call(PetscDrawDestroy(&draw));
  petsc_call(PetscViewerDestroy(&fd));
  petsc_call(MatDestroy(&A));

  petsc_call(PetscSNPrintf(command, PETSC_MAX_PATH_LEN, "mv %s/%s_0.xpm ./%s.xpm; rmdir %s", base, base, base, base));
  petsc_call(PetscPOpen(PETSC_COMM_SELF, NULL, command, "r", NULL));
  petsc_call(PetscFinalize());
  
  
  return 0;
}

