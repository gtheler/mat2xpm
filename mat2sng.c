/* mat2sng
 * author: jeremy theler <gtheler@cites-gss.com>
 * license: GPL v3+
 */
static char help[] = "\
mat2sng\n\
usage: ./mat2sng <matrix file>\n\n\
converts a PETSc binary matrix file into an SNG (scriptable network graphics)\n\
file which can be converted into a PNG image containing a representation of\n\
the non-zero structure in a pixel-by-element correspondence (i.e a MxN matrix\n\
results in an MxN image)\n\
\n\
the standard output should be fed to the tool sng <http://sng.sourceforge.net>:\n\
\n\
    $ ./mat2sng matrix.bin | sng > matrix.png\n\n";

#include <petscsys.h>
#include <petscmat.h>
#include <petscdraw.h>

#define petsc_call(f) ierr=(f);CHKERRQ(ierr);


#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **argv)
{
  Mat               A;
  PetscViewer       fd;
  char              file[PETSC_MAX_PATH_LEN];
  PetscErrorCode    ierr;
  PetscMPIInt       rank;
  PetscInt m, n, i, j;
  PetscScalar xi;

  PetscInitialize(&argc, &argv, (char*)0, help);
  petsc_call(MPI_Comm_rank(PETSC_COMM_WORLD,&rank));

  if (!rank) {
    if (argc != 2 || argv[1][0] == '-') {
      if (argv[1][0] != '-') {
        printf("%s", help);
      }
      return 0;
    }
    petsc_call(PetscStrncpy(file, argv[1], PETSC_MAX_PATH_LEN-1));
  
    petsc_call(PetscViewerBinaryOpen(PETSC_COMM_WORLD, file, FILE_MODE_READ, &fd));
    petsc_call(MatCreate(PETSC_COMM_WORLD, &A));
    petsc_call(MatSetFromOptions(A));
    petsc_call(MatLoad(A, fd));
    petsc_call(MatGetSize(A, &m, &n));

    printf("#SNG: PETSc matrix\n");
    printf("\n"); 
    printf("IHDR: {\n");
    printf("  width: %d;\n", n);
    printf("  height: %d;\n", m);
    printf("  bitdepth: 8;\n");
    printf("  using color: palette;\n");
    printf("}\n"); 
    printf("\n"); 
    printf("PLTE: {\n"); 
    printf("  (255, 255, 255)\n");
    printf("  (255,   0,   0)\n");
    printf("  (  0,   0, 255)\n");
    printf("}\n"); 
    printf("\n"); 
    printf("IMAGE: {\n"); 
    printf("  pixels base64\n"); 
  
    for (i = 0; i < m; i++) {
      for (j = 0; j < n; j++) {
        petsc_call(MatGetValues(A, 1, &i, 1, &j, &xi));
        if (xi == 0) {
          printf("0");
        } else if (xi > 0) {
          printf("1");
        } else {
          printf("2");
        }
      }
      printf("\n");
    }
    printf("}\n"); 
    
    petsc_call(MatDestroy(&A));
    petsc_call(PetscViewerDestroy(&fd));

  }
  
  petsc_call(PetscFinalize());
    
  return 0;
}
